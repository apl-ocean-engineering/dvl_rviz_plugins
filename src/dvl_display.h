/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DVL_DISPLAY_H_
#define DVL_DISPLAY_H_

#include <boost/circular_buffer.hpp>

#include <acoustic_msgs/Dvl.h>
#include <rviz/message_filter_display.h>

namespace Ogre
{
class SceneNode;
}

namespace rviz
{
class ColorProperty;

class FloatProperty;

class IntProperty;
}

// All the source in this plugin is in its own namespace.  This is not
// required but is good practice.
namespace rviz_plugin_dvl
{
class DvlVelVisual;  // forward declaration
class DvlVelDisplay : public rviz::MessageFilterDisplay<acoustic_msgs::Dvl>
{
  Q_OBJECT
public:
  DvlVelDisplay();

  virtual ~DvlVelDisplay() override;

protected:
  virtual void onInitialize();

  // helpter to clear display back to its original state
  virtual void reset();

private Q_SLOTS:

  void updateColorAndAlpha();

  void updateHistoryLength();

private:
  void processMessage(const acoustic_msgs::Dvl::ConstPtr& msg);

  // storage for a list of visuals
  boost::circular_buffer<boost::shared_ptr<DvlVelVisual> > visuals_;

  /// user-editable property values
  rviz::ColorProperty* color_property_;
  rviz::FloatProperty* alpha_property_;
  rviz::IntProperty* history_length_property_;
};  // class DvlVelDisplay

class DvlRangeVisual;  // forward declaration
class DvlRangeDisplay : public rviz::MessageFilterDisplay<acoustic_msgs::Dvl>
{
  Q_OBJECT
public:
  DvlRangeDisplay();

  ~DvlRangeDisplay() override;

protected:
  virtual void onInitialize();

  // helpter to clear display back to its original state
  virtual void reset();

private Q_SLOTS:

  void updateColorAndAlpha();

  void updateHistoryLength();

private:
  void processMessage(const acoustic_msgs::Dvl::ConstPtr& msg);

  // storage for a list of visuals
  boost::circular_buffer<boost::shared_ptr<DvlRangeVisual> > visuals_;

  /// user-editable property values
  rviz::ColorProperty* color_property_;
  rviz::FloatProperty* alpha_property_;
  rviz::IntProperty* history_length_property_;
};  // class DvlRangeDisplay

};  // namespace rviz_plugin_dvl

#endif /* DVL_DISPLAY_H_ */
