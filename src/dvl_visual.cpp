/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 12/7/17.
//

#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <rviz/ogre_helpers/arrow.h>
#include <rviz/ogre_helpers/shape.h>
#include <geometry_msgs/Vector3.h>

#include "dvl_visual.h"

namespace rviz_plugin_dvl
{
// BEGIN_TUTORIAL
DvlVelVisual::DvlVelVisual(Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node)
{
  scene_manager_ = scene_manager;

  // Ogre::SceneNode s form a tree, with each node storing the
  // transform (position and orientation) of itself relative to its
  // parent.  Ogre does the math of combining those transforms when it
  // is time to render.
  //
  // Here we create a node to store the pose of the Dvl's header frame
  // relative to the RViz fixed frame.
  frame_node_ = parent_node->createChildSceneNode();

  // We create the arrow object within the frame node so that we can
  // set its position and direction relative to its header frame.
  velocity_arrow_.reset(new rviz::Arrow(scene_manager_, frame_node_));
}

DvlVelVisual::~DvlVelVisual()
{
  // Destroy the frame node since we don't need it anymore.
  scene_manager_->destroySceneNode(frame_node_);
}

void DvlVelVisual::setMessage(const acoustic_msgs::Dvl::ConstPtr& msg)
{
  const geometry_msgs::Vector3& v = msg->velocity;

  // Convert the geometry_msgs::Vector3 to an Ogre::Vector3.
  Ogre::Vector3 vel(v.x, v.y, v.z);

  // Find the magnitude of the acceleration vector.
  float length = vel.length();

  // Scale the arrow's thickness in each dimension along with its length.
  Ogre::Vector3 scale(length, length, length);
  velocity_arrow_->setScale(scale);

  // Set the orientation of the arrow to match the direction of the
  // velocity vector.
  velocity_arrow_->setDirection(vel);
}

// Position and orientation are passed through to the SceneNode.
void DvlVelVisual::setFramePosition(const Ogre::Vector3& position)
{
  frame_node_->setPosition(position);
}

void DvlVelVisual::setFrameOrientation(const Ogre::Quaternion& orientation)
{
  frame_node_->setOrientation(orientation);
}

// Color is passed through to the Arrow object.
void DvlVelVisual::setColor(float r, float g, float b, float a)
{
  velocity_arrow_->setColor(r, g, b, a);
}
// END_TUTORIAL

// 2 deg
const double BEAM_WIDTH_RAD = 0.069813170;

// BEGIN_TUTORIAL
DvlRangeVisual::DvlRangeVisual(Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node)
{
  scene_manager_ = scene_manager;

  // Ogre::SceneNode s form a tree, with each node storing the
  // transform (position and orientation) of itself relative to its
  // parent.  Ogre does the math of combining those transforms when it
  // is time to render.
  //
  // Here we create a node to store the pose of the Dvl's header frame
  // relative to the RViz fixed frame.
  frame_node_ = parent_node->createChildSceneNode();

  // We create the arrow object within the frame node so that we can
  // set its position and direction relative to its header framer
  range_cones_.resize(4);
  for (size_t i = 0; i < range_cones_.size(); i++)
  {
    range_cones_[i] = new rviz::Shape(rviz::Shape::Cone, scene_manager, frame_node_);
  }
}

DvlRangeVisual::~DvlRangeVisual()
{
  // Destroy the frame node since we don't need it anymore.
  for (auto& cone : range_cones_)
  {
    delete cone;
  }
  scene_manager_->destroySceneNode(frame_node_);
}

void DvlRangeVisual::setMessage(const acoustic_msgs::Dvl::ConstPtr& msg)
{
  for (size_t i = 0; i < msg->range.size(); i++)
  {
    Ogre::Real disp_range;
    if (std::isnan(msg->range[i]))
    {
      disp_range = 0.0;
    }
    else
    {
      disp_range = msg->range[i];
    }

    const geometry_msgs::Vector3& uv = msg->beam_unit_vec[i];

    // Convert the geometry_msgs::Vector3 to an Ogre::Vector3.
    // Recover the rotation
    Ogre::Vector3 beamvec(uv.x, uv.y, uv.z);
    Ogre::Vector3 basevec(0, -1.0, 0);  // orientation of base cone
    Ogre::Quaternion orientation = basevec.getRotationTo(beamvec);

    float width = 2.0 * disp_range * tan(BEAM_WIDTH_RAD / 2.0);
    // std::cout <<"RANGE: " <<disp_range <<" WIDTH: " <<width <<std::endl;
    // Scale the arrow's thickness in each dimension along with its length.
    Ogre::Vector3 scale(width, disp_range, width);
    // Ogre::Vector3 scale(msg->range[i], width, msg->range[i]);
    // Ogre::Vector3 scale(msg->range[i], msg->range[i], msg->range[i]);

    // The position is really the center of the array
    // Running on Noetic, it appears that this fudge factor is no longer
    // necessary.
    // 0.00824 fudge factor measured.  See range_display.cpp in RViz
    // Ogre::Vector3 position(0, -(disp_range / 2.0 - 0.008824 * disp_range), 0);
    Ogre::Vector3 position(0, -(disp_range / 2.0), 0);
    position = orientation * position;

    range_cones_[i]->setScale(scale);
    range_cones_[i]->setOrientation(orientation);
    range_cones_[i]->setPosition(position);

  }  // for each beam
}

// Position and orientation are passed through to the SceneNode.
void DvlRangeVisual::setFramePosition(const Ogre::Vector3& position)
{
  frame_node_->setPosition(position);
}

void DvlRangeVisual::setFrameOrientation(const Ogre::Quaternion& orientation)
{
  frame_node_->setOrientation(orientation);
}

// Color is passed through to the Arrow object.
void DvlRangeVisual::setColor(float r, float g, float b, float a)
{
  for (auto& cone : range_cones_)
  {
    cone->setColor(r, g, b, a);
  }
}
// END_TUTORIAL

}  // end namespace rviz_plugin_tutorials
